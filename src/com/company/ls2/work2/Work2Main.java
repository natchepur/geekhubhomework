package com.company.ls2.work2;

import java.util.Scanner;

public class Work2Main {
    private static Shapes shape;

    public enum Shapes {
        CIRCLE, SQUARE, RECTANGLE, TRIANGLE
    }

    public static void main (String args[]){

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите фигуру для измерений");

        String sShape = sc.nextLine();

        try {
            shape = Shapes.valueOf(sShape);
        }
        catch (IllegalArgumentException e) {
            System.out.println("Введенной Вами фигуры не существует в тестовом методе!");
        }

        switch(shape) {
            case CIRCLE:
                System.out.println("Введите радиус круга");
                int r = sc.nextInt();
                Circle circle = new Circle(r,0,0,0);

                System.out.println("Площадь круга равна "+circle.calculateArea());
                System.out.println("Периметр круга равен "+circle.calculatePerimeter());

                break;
            case SQUARE:
                System.out.println("Введите размер стороны квадрата");
                int i = sc.nextInt();
                Square square = new Square(i,i,i,i);

                System.out.println("Площадь квадрата равна "+square.calculateArea());
                System.out.println("Периметр квадрата равен "+square.calculatePerimeter());

                System.out.println("Квадрат состоит из двух треугольников, площадь каждой из которых равна "+square.getAreaTriangleOfRectangle()
                        +" и периметр каждого из которых равен "+square.getPerimeterTriangleOfRectangle());
                break;
            case RECTANGLE:
                System.out.println("Введите размеры двух сторон прямоугольника");
                int a = sc.nextInt();
                int b = sc.nextInt();
                Rectangle rectangle = new Rectangle(a,b,a,b);

                System.out.println("Площадь прямоугольника равна "+rectangle.calculateArea());
                System.out.println("Периметр прямоугольника равен "+rectangle.calculatePerimeter());

                System.out.println("Прямоугольник состоит из двух треугольников, площадь каждой из которых равна "+rectangle.getAreaTriangleOfRectangle()
                        +" и периметр каждого из которых равен "+rectangle.getPerimeterTriangleOfRectangle());
                break;
            case TRIANGLE:
                System.out.println("Введите размеры трех сторон треугольника и размер высоты (для определения площпди)");
                a = sc.nextInt();
                b = sc.nextInt();
                int c = sc.nextInt();
                int h = sc.nextInt();
                Triangle triangle = new Triangle(a,b,c,h);

                System.out.println("Площадь треугольника равна "+triangle.calculateArea());
                System.out.println("Периметр треугольника равен "+triangle.calculatePerimeter());
                break;
            default:
                System.out.println("");
                break;
        }

    }
}

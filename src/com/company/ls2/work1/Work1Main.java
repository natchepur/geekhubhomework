package com.company.ls2.work1;

public class Work1Main {

    private static Type type;

    public static void main(String[] args) {

        // Машина
       // Engine engineCar = new Engine(10, 100);
       // TripComputer tripComputerCar = new TripComputer();
        Vehicles car = new Vehicles(Type.CITY_CAR,5, new Engine(10, 100), new TripComputer());
        System.out.println("Vehicles is:\n" + car.getType());
        //car.setFuel(20.5);
        car.getTripComputer().fuelLevel();
        car.getEngine().on();
        car.getEngine().go(250);
        car.getTripComputer().fuelLevel();
        car.getTripComputer().condition();

        // Гоночный автомобиль
        SportsCar sportsCar = new SportsCar(Type.SPORTS_CAR,2, new Engine(25, 10000), new TripComputer(), 250);
        System.out.println("Vehicles is:\n" + sportsCar.getType());
       // sportsCar.setFuel(10);
        sportsCar.getTripComputer().condition();
        sportsCar.getTripComputer().fuelLevel();
        sportsCar.increaseSpeed(50);
        sportsCar.getEngine().on();
        sportsCar.getTripComputer().condition();
        sportsCar.getEngine().go(250);
        sportsCar.increaseSpeed(350);

        // Лодка
       // Engine engineBoat = new Engine(10, 100);
       // TripComputer tripComputerBoat = new TripComputer();
        Boat boat = new Boat(Type.BOAT,5, new Engine(10, 100),  new TripComputer(), 7);
        System.out.println("Vehicles is:\n" + boat.getType());
       // boat.setFuel(200);

        boat.getEngine().on();
        boat.hoistSail();
        boat.getEngine().go(500);
        boat.getTripComputer().fuelLevel();
        boat.getTripComputer().condition();

        boat.getUpSail();
        boat.getSailHeight();

        SunCar sunCar = new SunCar(Type.SUN_CAR, 4, new Engine(10, 100),  new TripComputer(), 200, 100);
        sunCar.increaseSpeed(100);
        sunCar.getTripComputer().condition();


    }



}
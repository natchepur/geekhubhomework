package com.company.ls3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedLst{

    public static void createLinkedLst() {
        LinkedList<String> alphabet = new LinkedList<>();

        alphabet.add("B");
        alphabet.add("C");
        alphabet.add("D");
        alphabet.addFirst("A");
        alphabet.addLast("E");
        System.out.println("Element with index 3: " + alphabet.get(1));
        alphabet.remove("D");
        alphabet.size();
        System.out.println("The size is: " + alphabet.size());
        System.out.println(alphabet);
        System.out.println("The first item in the list is " + alphabet.getFirst());
        System.out.println("The last item in the list is " + alphabet.getLast());
        alphabet.set(2, "CC");
        alphabet.add(3, "D");
        System.out.println(alphabet);

        for (String anAlphabet : alphabet) {
            System.out.println(anAlphabet);
        }

        ListIterator<String> litr = alphabet.listIterator();
        while (litr.hasNext()) {
            litr.set(litr.next() + "+");
        }

        while (litr.hasPrevious()) {
            System.out.println(litr.previous());
        }
    }
}


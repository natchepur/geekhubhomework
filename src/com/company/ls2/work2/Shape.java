package com.company.ls2.work2;

abstract class Shape {
    double a;
    double b;
    double c;
    double d;

    Shape(double sizeA, double sizeB, double sizeC, double sizeD){
        a = sizeA;
        b = sizeB;
        c = sizeC;
        d = sizeD;
    }

    abstract double calculateArea();
    abstract double calculatePerimeter();

    double getAreaTriangleOfRectangle(){
        return a*b/2;
    }

    double getPerimeterTriangleOfRectangle(){
        return (a+b+c+d)/2;
    }

}

class Rectangle extends Shape {

    Rectangle(double sizeA, double sizeB, double sizeC, double sizeD){
        super(sizeA,sizeB,sizeC,sizeD);
    }

    @Override
    double calculateArea() {
        return a * b;
    }
    @Override
    double calculatePerimeter() {
        return (a * 2) + (b * 2);
    }

}

class Triangle extends Shape {

    Triangle(double sizeA, double sizeB, double sizeC, double sizeH){
        super(sizeA,sizeB,sizeC,sizeH);
    }

    @Override
    double calculateArea() {
        return c * d; // высота на основание
    }
    @Override
    double calculatePerimeter() {
        return a + b + c;
    }

}

class Circle extends Shape {

    Circle(double sizeA, double sizeB, double sizeC, double sizeD){
        super(sizeA,sizeB,sizeC,sizeD);
    }

    @Override
    double calculateArea() {
        return a * 3.14 * a; // Площадь круга π*R*R
    }
    @Override
    double calculatePerimeter() {  // Периметр круга 2ПR
        return a * 2 * 3.14;
    }

}

class Square extends Shape {

    Square(double sizeA, double sizeB, double sizeC, double sizeD){
        super(sizeA,sizeB,sizeC,sizeD);
    }

    @Override
    double calculateArea() {
        return a * a; // Площадь круга π*R*R
    }
    @Override
    double calculatePerimeter() {  // Периметр круга 2ПR
        return a * 4;
    }

}
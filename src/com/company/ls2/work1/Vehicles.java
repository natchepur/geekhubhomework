package com.company.ls2.work1;

// транспотное средство
class Vehicles {
    /*
        private final Type type;
        private final int seats;
        private final Engine engine;
        private final TripComputer tripComputer;
        private static double fuel = 0;
        */
    private final Type type;
    private int seats;
    private Engine engine;
    private TripComputer tripComputer;
    private static double fuel = 0;

    Vehicles(Vehicles ob){
        type = ob.type;
        seats = ob.seats;
        engine = ob.engine;
        tripComputer = ob.tripComputer;
    }

    Vehicles(Type type, int seats, Engine engine, TripComputer tripComputer) {
        this.type = type;
        this.seats = seats;
        this.engine = engine;
        this.tripComputer = tripComputer;
     }

     // топливо
    static void setFuel(double fl) {
        fuel = fl;
    }

    static double getFuel() {
        return fuel;
    }

    Type getType() {
        return type;
    }

    public int getSeats() {
        return seats;
    }

    // двигатель
    Engine getEngine() {
        return engine;
    }

    TripComputer getTripComputer() {
        return tripComputer;
    }


}




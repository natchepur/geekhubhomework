package com.company.ls2.work1;

class SunCar extends SportsCar {
    private double chargeLevel = 0; // Уровень заряда

    SunCar(SunCar ob){
        super(ob);
        chargeLevel = ob.chargeLevel;
    }

    SunCar(Type type, int seats, Engine engine, TripComputer tripComputer, double maxSpeed, double chargeLevel) {
        super(type, seats, engine, tripComputer, maxSpeed);
        this.chargeLevel = chargeLevel;
    }

    public double getChargeLevel() {
        return chargeLevel;
    }

    public void setChargeLevel(double chargeLevel) {
        this.chargeLevel = chargeLevel;
    }
}

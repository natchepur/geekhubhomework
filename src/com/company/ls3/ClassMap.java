 package com.company.ls3;

import java.util.HashMap;
import java.util.Map;

public class ClassMap {
    public static void createMap() {
        Map<String, String> hashmap = new HashMap <>();
        hashmap.put("0", "Ivanov");
        hashmap.put("1", "Petrov");
        hashmap.put("2", "Sidorov");
        hashmap.put("3", "Dvorov");

        //hashmap.get("1");
        System.out.println(hashmap);
        System.out.println("Element with key 1: " + hashmap.get("1"));
        System.out.println("The size is: " + hashmap.size());

        hashmap.remove("3");
        System.out.println(hashmap);

        for( Map.Entry<String, String> entry : hashmap.entrySet() ){
            System.out.println( entry.getKey() + " " + entry.getValue() );
        }
    }
}

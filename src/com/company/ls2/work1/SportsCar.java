package com.company.ls2.work1;

public class SportsCar extends Vehicles {

    private double maxVelocity = 0;  // наибольшую скорость автомобиля
    private double speed = 0; // текущую скорость автомобиля

    SportsCar(SportsCar ob){
        super(ob);
        maxVelocity = ob.maxVelocity;
    }

    SportsCar(Type type, int seats, Engine engine, TripComputer tripComputer, double maxSpeed) {
        super(type, seats, engine, tripComputer);
        maxVelocity = maxSpeed;
    }

    // Набрать скорость
    void increaseSpeed(double spd){

        if (!Engine.isStarted()){
            System.err.println("Start the car!");
            return;
        }
        if ((speed + spd) > maxVelocity){
            System.err.println("The desired speed exceeds the maximum!");
            System.err.println("The maximum speed is "+maxVelocity+"!");
        }
        else {
            speed += spd;
            System.out.println("The current speed is "+speed+"!");
        }
    }

    public void getMaxVelocity() {
        System.out.println("The maximum speed is "+maxVelocity+"!");
    }

    public void getSpeed() {
        System.out.println("The current speed is "+speed+"!");
    }

}

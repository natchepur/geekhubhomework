package com.company.ls2.work1;

class TripComputer {

    void condition() {
        if (Engine.isStarted()) {
            System.out.println("Vehicle is started");
        } else {
            System.out.println("Vehicle isn't started");
        }
    }

    void fuelLevel() {
        System.out.println("Level of fuel - " + Vehicles.getFuel());
    }

}

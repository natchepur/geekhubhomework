package com.company.ls2.work1;

// Двигатель
class Engine {
    private final double volume;
    private double mileage = 0;
    private static boolean started = false;

    Engine(double volume, double mileage) {
        this.volume = volume;
        this.mileage = mileage;
    }

    void on() {
        started = true;
    }

    public void of() {
        started = false;
    }

    static boolean isStarted() {
        return started;
    }

    void go(double mileage) {
        if (started) {
            this.mileage += mileage;
            if (!fuelConsumption(this.mileage)){
                System.err.println("For such a distance, there is not enough fuel!");
            }
        } else {
            System.err.println("Cannot go(), you must start engine first!");
        }
    }

    private boolean fuelConsumption(double mileage){
        double cons = this.mileage/10;
        double fl = Vehicles.getFuel();

        if (cons>fl) return false;

        Vehicles.setFuel(fl-cons);
        return true;
    }

    public double getVolume() {
        return volume;
    }

    public double getMileage() {
        return mileage;
    }
}

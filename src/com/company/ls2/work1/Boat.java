package com.company.ls2.work1;

// Лодка
class Boat extends Vehicles {

    private double sailHeight; //высота паруса
    private boolean upSail = false;

    Boat(Type type, int seats, Engine engine, TripComputer tripComputer, double height) {
        super(type, seats, engine, tripComputer);
        sailHeight = height;
    }


    // Поднять паруса
    void hoistSail(){
        upSail = true;
    }

    // Опустить паруса
    public void downSail(){
        upSail = false;
    }

    void getUpSail(){
        if (upSail) {
            System.out.println("Boat's sail is hoisted");
        }
        else {
            System.out.println("Boat's sail is not hoisted");
        }
    }

    void getSailHeight(){
        System.out.println("Sail height is "+sailHeight+" meters");
    }
}

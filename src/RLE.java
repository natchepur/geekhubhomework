import java.util.Objects;
import java.util.Scanner;

public class RLE {

    public static void main(String[] args) {
        int k=1;
        StringBuilder strMsg = new StringBuilder();

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите строку из повторяющихся символов");
        String str = sc.nextLine();

        char[] chars = str.toCharArray();
        char chOld='j';
        for (int j = 0; j < chars.length; j++) {
           if (j==0){
               chOld = chars[j];
           }
            else if(chOld == chars[j]) {
                k++;
            }
            else {

               strMsg.append(Integer.toString(k));
               strMsg.append(chOld);
               chOld = chars[j];
               k=1;
           }
        }
        strMsg.append(Integer.toString(k));
        strMsg.append(chOld);
        System.out.println(strMsg);
    }
}

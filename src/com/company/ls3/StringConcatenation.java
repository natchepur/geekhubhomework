package com.company.ls3;

import java.util.Random;

public class StringConcatenation {

    private static int lengthStr = 30;

    public static void main(String[] args) {

        String validCharacters =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        long start = System.nanoTime();
        String str = generateRandomChars(validCharacters, lengthStr);
        for (int i = 0; i < 100; i++) {
            str = str.concat(generateRandomChars(validCharacters, lengthStr));
        }
        long end = System.nanoTime();
        double traceTimeString = (end-start)/100000;
        System.out.println("String: "+traceTimeString);

        start = System.nanoTime();
        StringBuffer sBuf = new StringBuffer(generateRandomChars(validCharacters, lengthStr));
        for (int i = 0; i < 100; i++) {
            sBuf.append(generateRandomChars(validCharacters, lengthStr));
        }
        end = System.nanoTime();
        double traceTimeBuf = (end-start)/100000;
        System.out.println("StringBuffer: "+traceTimeBuf);

        start = System.nanoTime();
        StringBuilder sBuild = new StringBuilder(generateRandomChars(validCharacters, lengthStr));
        for (int i = 0; i < 100; i++) {
            sBuild.append(generateRandomChars(validCharacters, lengthStr));
        }
        end = System.nanoTime();
        double traceTimeBuild = (end-start)/100000;
        System.out.println("StringBuilder: "+traceTimeBuild);

        for(int x = 10; x >= 0; x--){
            for(int y = 0; y <= 20; y++){
               if (y==1 && x*5<traceTimeString){
                   // String
                   System.out.print('|');
               }
               else if (y==10 && x*5<traceTimeBuf){
                    // StringBuffer
                    System.out.print('|');
               }
               else if (y==19 && x*5<traceTimeBuild){
                    // StringBuilder
                    System.out.print('|');
               }
               else{
                   if (x==0){
                       System.out.print('_');
                   }
                   else{
                       System.out.print(' ');
                   }
               }
            }
            System.out.println();
        }

        for(int y = 1; y <= 20; y++){
            if (y==1){
                System.out.print(traceTimeString);
            }
            else if (y==10){
                System.out.print(traceTimeBuf);
            }
            else if (y==19){
                System.out.print(traceTimeBuild);
            }
            else{
                System.out.print(' ');
            }
        }
    }

    private static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars
                    .length())));
        }

        return sb.toString();
    }
}
